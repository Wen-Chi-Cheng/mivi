//
//  LabelTableViewCell.swift
//  FasterEats
//
//  Created by TingKuan Lin on 2018/8/28.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell {

    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
