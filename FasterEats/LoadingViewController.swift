//
//  LoadingViewController.swift
//  FasterEats
//
//  Created by TingKuan Lin on 2018/8/28.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    
    @IBOutlet weak var loadingView: UIImageView!
    var timer: Timer?
    var timeInterval:Int = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadingView.loadGif(name: "loading")
        
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(tickDown), userInfo: nil, repeats: true)
        
        
        //_ = Timer.scheduledTimer(timeInterval: 1,target:self,selector:Selector(("tickDown")),userInfo:nil,repeats:true)
        //RunLoop.current.add(timer, forMode: .defaultRunLoopMode)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func tickDown()
    {
        print("\(timeInterval)")
        if(timeInterval == 0){
            print("Time Out !!")
            timer?.invalidate()
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "mainVC")
            present(controller!, animated: true, completion: nil)
            
        }else{
            timeInterval -= 1
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
