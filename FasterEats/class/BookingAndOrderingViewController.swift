//
//  BookingAndOrderingViewController.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/26.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit
import Foundation

class BookingAndOrderingViewController: UIViewController {
    
    var resImageView:String = ""
    var restaurantName:String = ""
    var star:String = ""
    var date:String = ""
    var time:String = ""
    var peopleNumber:String = ""
    var meal:[String] = []
    var mealPrice:[String] = []
    var mealCount:[String] = []
    var totalPrice:Int = 0
    var totalMeal:String = ""
    
    
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var starLabel: UILabel!
    //@IBOutlet weak var dateLabel: UILabel!
    //@IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var peopleNumberLabel: UILabel!
    @IBOutlet weak var mealLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmButton(_ sender: Any) {
        let confirmVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmVC") as! ConfirmViewController
        
        confirmVC.resName = self.restaurantName
        confirmVC.star = self.star
        confirmVC.date = self.date
        confirmVC.time = self.time
        confirmVC.peopleNum = self.peopleNumber
        confirmVC.meal = self.totalMeal
        confirmVC.price = self.totalPrice
        
        present(confirmVC, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countTotalPrice()
        printMeal()
        
        mealLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        mealLabel.numberOfLines = 0
        
        restaurantImageView.image = UIImage(named: resImageView)
        restaurantNameLabel.text = restaurantName
        starLabel.text = star
        //dateLabel.text = date
        //timeLabel.text = time
        peopleNumberLabel.text = peopleNumber
        totalPriceLabel.text = "\(totalPrice)"
        mealLabel.text = totalMeal
        
        
        print(totalMeal)
        //print("date: \(dateLabel.text)\ntime: \(timeLabel.text)")
//        for index in 0...meal.count {
//            if meal[index] == ""{
//                break;
//            }
//            else{
//                print("meal: \(meal[index])")
//                print("mealPrice: \(mealPrice[index])")
//                print("mealCount: \(mealCount[index])")
//            }
//        }
        
        title = "確認訂餐及訂位"
        //UINavigationBar.appearance().tintColor = UIColor.fadedOrange
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.fadedOrange]
        //UINavigationBar.appearance().titleTextAttributes = [NSForegroundAttributeName: UIColor.orange]
        
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: UIImage(named: "fill3"), style: .done, target: self, action: #selector(BookingAndOrderingViewController.back))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "fill3"), style: .done, target: self, action: #selector(BookingAndOrderingViewController.back))
        
        
        
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Collect", style: .done, target: self, action: #selector(BookingAndOrderingViewController.collect))
        
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(ViewController.playWith(arg:)))

        // Do any additional setup after loading the view.
    }
    
    func countTotalPrice(){
        for i in 0...meal.count {
            if meal[i] == "" {
                break;
            }
            else {
                var mealPriceInt:Int? = Int(mealPrice[i])
                var mealCountInt:Int? = Int(mealCount[i])
                //print("mealPrice[i] = \(mealPrice[i])")
                if mealCountInt != nil{
                    print("meal price int:\(mealPriceInt), meal count int\(mealCountInt)")
                    totalPrice += mealPriceInt! * mealCountInt!
                }
                
            }
        }
    }
    
    func printMeal(){
        for i in 0...meal.count {
            if meal[i] == "" {
                break;
            }
            else {
                if mealCount[i] != "" {
                    totalMeal += meal[i] + " *" + mealCount[i] + "\n"
                }
            }
        }
    }
    
    @objc func back(){
        
        print("back")
        dismiss(animated: true, completion: nil)
    }
    
    @objc func collect(){
        print("collect")
    }
    
//    @objc func playWith(arg:Any){
//        print("playWith item")
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
