//
//  MapViewController.swift
//  FasterEats
//
//  Created by TingKuan Lin on 2018/8/24.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: ViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var mapData:[RestaurantMap] = [
        RestaurantMap(id:-1, name: "MY麵屋", address: "116台北市文山區指南路二段45巷7號", lat: 24.9883459, lon: 121.574246, color: UIColor.red, type: "麵食", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:2,name: "四川飯館", address: "116台北市文山區指南路二段65號2樓", lat: 24.9879254, lon: 121.5747746, color: UIColor.red, type: "盒菜", price:"NT$~$$", dist:"200m", seat:"尚有5位"),
        RestaurantMap(id:1,name: "金鮨日式料理", address: "116台北市文山區指南路二段205號", lat: 24.9869622, lon: 121.578674, color: UIColor.red, type: "日式料理", price:"NT$~$$", dist:"600m", seat:"已客滿"),
        RestaurantMap(id:-1,name: "蝴蝶漾中西創意料理", address: "116台北市文山區指南路二段161號", lat: 24.9874138, lon: 121.5774972, color: UIColor.red, type: "創意料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:0,name: "Juicy Bun Burger", address: "116台北市文山區萬壽路19號1樓", lat: 24.9882457, lon: 121.5760434, color: UIColor.red, type: "美式餐廳", price:"NT$~$$", dist:"350m", seat:"有空位"),
        RestaurantMap(id:-1,name: "明池豆花", address: "116台北市文山區指南路二段56號1樓", lat: 24.9876087, lon: 121.5754459, color: UIColor.red, type: "甜點", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "樂山食堂", address: "116台北市文山區新光路一段26號", lat: 24.989007, lon: 121.573557, color: UIColor.red, type: "日式料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "加賀日式料理", address: "116台北市文山區指南路二段17號1樓", lat: 24.988308, lon: 121.573499, color: UIColor.red, type: "日式料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "江記水盆羊肉", address: "116台北市文山區指南路二段45巷12號", lat: 24.988768, lon: 121.574557, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "鄰居家 NEXT DOOR CAFE 政大店", address: "116台北市文山區指南路二段64號憩賢樓", lat: 24.986150406074565, lon: 121.57715857028961, color: UIColor.red, type: "餐廳", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "江記水盆羊肉", address: "116台北市文山區指南路二段45巷12號", lat: 24.988768, lon: 121.574557, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "敏忠小吃店", address: "116台北市文山區指南路二段57號1樓", lat: 24.9879813, lon: 121.574567, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "甘泉魚麵(政大店)", address: "116台北市文山區新光路一段34號", lat: 24.9893179, lon: 121.5736, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "三媽臭臭鍋（新光店）", address: "116台北市文山區新光路一段117號", lat: 24.991361, lon: 121.573818, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "揉道Nubun不老麵糰", address: "116台北市文山區新光路一段143號", lat: 24.991961, lon: 121.573884, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "松町和風小舖", address: "116台北市文山區木新路二段60號", lat: 24.986033, lon: 121.570516, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        
        RestaurantMap(id:-1,name: "阿義師牛肉麵", address: "116台北市文山區指南路二段219號", lat: 24.9866897, lon: 121.5789727, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "東京小城", address: "116台北市文山區指南路二段207號", lat: 24.9869282, lon: 121.5786804, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "shabu鮮涮涮鍋", address: "116台北市文山區萬壽路23號", lat: 24.9882596, lon: 121.5761296, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1,name: "Lecker里克德義廚房", address: "116台北市文山區新光路一段22-1號", lat: 24.9888679, lon: 121.573615, color: UIColor.red, type: "料理", price:"NT$~$$", dist:"", seat:""),
        RestaurantMap(id:-1, name: "我在這裡", address: "116台北市文山區指南路二段64號", lat: 24.9864564, lon: 121.5756198, color: UIColor.red, type: "", price:"", dist:"", seat:"")
        
    ]
    var color:[UIColor] = [UIColor.red, UIColor.orange, UIColor.green, UIColor.gray]
    
    
    @IBOutlet weak var mapView: MKMapView!
    var location : CLLocationManager!;
    
    @IBAction func move(_ sender: Any) {
        MoveToUserLocation()
    }
    
    @IBAction func BackToList(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        location = CLLocationManager();
        location.delegate = self;
        
        //詢問使用者是否同意給APP定位功能
        location.requestWhenInUseAuthorization();
        //開始接收目前位置資訊
        location.startUpdatingLocation();
        location.distanceFilter = CLLocationDistance(10);
        
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.showsUserLocation = true
        mapView.isZoomEnabled = true
        mapView.showsCompass = true; // 是否显示指南针
        mapView.showsScale = true; // 是否显示比例尺
        
        // 地圖預設顯示的範圍大小 (數字越小越精確)
        let latDelta = 0.05
        let longDelta = 0.05
        let currentLocationSpan:MKCoordinateSpan =
            MKCoordinateSpanMake(latDelta, longDelta)
        
        // 設置地圖顯示的範圍與中心點座標
        let center:CLLocation = CLLocation(
            latitude: 24.9864564, longitude: 121.5756198)
        let currentRegion:MKCoordinateRegion =
            MKCoordinateRegion(
                center: center.coordinate,
                span: currentLocationSpan)
        mapView.setRegion(currentRegion, animated: true)
        
        
        AddPoint()
        MoveToUserLocation()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func MoveToUserLocation(){
        
        //let coordination:CLLocationCoordinate2D = mapView.userLocation.coordinate
        
        let coordination:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 24.987450267332587,longitude: 121.5747720756408)
        
        let mySpan = MKCoordinateSpanMake(0.008, 0.008)
        
        let toRegion = MKCoordinateRegionMake(coordination, mySpan)
        mapView.setRegion(toRegion, animated:true)
        
    }
    
    func AddPoint(){
        
        
        var annotations:[MKAnnotation] = []
        
        for restaurant:RestaurantMap in mapData {
            
            var objectAnnotation = MKPointAnnotation()
            print("\(restaurant.name)")
            
            objectAnnotation.coordinate = CLLocation(
                latitude: restaurant.lat,
                longitude: restaurant.lon).coordinate
            objectAnnotation.title = restaurant.name
            objectAnnotation.subtitle = restaurant.price + "\t|   " + restaurant.dist + "\n" + restaurant.seat
            
            annotations.append(objectAnnotation)
            //mapView.addAnnotation(objectAnnotation)
        }
        print("\(annotations.count)")
        mapView.addAnnotations(annotations)
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        var target:String = ((view.annotation?.title)!)!
        var index:Int = -1
        for tmp in mapData{
            if tmp.name == target{
                index = tmp.id
            }
        }
        if(index != -1){
            let resInfo = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantInfoVC") as! RestaurantInformationViewController
            
            resInfo.restaurantIndex = index
            present(resInfo, animated: true, completion: nil)
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "myAnnotation") as? MKMarkerAnnotationView
        
        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "myAnnotation")
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        if annotation is MKUserLocation {
            return nil
        }
        
        annotationView?.markerTintColor = color[Int(arc4random_uniform(UInt32(color.count)))]
        if annotation.coordinate.latitude == 24.9879254 && annotation.coordinate.longitude == 121.5747746 {
            
            annotationView?.markerTintColor = UIColor.orange
        }
        if annotation.coordinate.latitude == 24.9864564 && annotation.coordinate.longitude == 121.5756198 {
            
            annotationView?.markerTintColor = UIColor.blue
        }
        
        return annotationView
    }
    
    
    
    
    
    //    func mapView(_ map: MKMapView!, viewFor annotation: MKAnnotation!) -> MKAnnotationView! {
    //
    //        if annotation is MKUserLocation {
    //
    //            if annotation.coordinate.latitude == mapView.userLocation.coordinate.latitude && annotation.coordinate.longitude == mapView.userLocation.coordinate.longitude {
    //                return nil
    //            }
    //
    //            // 建立可重複使用的 MKAnnotationView
    //            let reuseId = "MyPin"
    //            var pinView =
    //                map.dequeueReusableAnnotationView(
    //                    withIdentifier: reuseId)
    //            if pinView == nil {
    //                // 建立一個地圖圖示視圖
    //                pinView = MKAnnotationView(
    //                    annotation: annotation,
    //                    reuseIdentifier: reuseId)
    //                // 設置點擊地圖圖示後額外的視圖
    //                pinView?.canShowCallout = true
    //                // 設置自訂圖示
    //                //pinView?.image = UIImage(named:"user")
    //            } else {
    //                pinView?.annotation = annotation
    //            }
    //            return pinView
    //
    //        } else {
    //
    //            // 建立可重複使用的 MKPinAnnotationView
    //            let reuseId = "Pin"
    //            var pinView =
    //                map.dequeueReusableAnnotationView(
    //                    withIdentifier: reuseId) as? MKPinAnnotationView
    //            if pinView == nil {
    //                // 建立一個大頭針視圖
    //                pinView = MKPinAnnotationView(
    //                    annotation: annotation,
    //                    reuseIdentifier: reuseId)
    //                // 設置點擊大頭針後額外的視圖
    //                pinView?.canShowCallout = true
    //                // 會以落下釘在地圖上的方式出現
    //                pinView?.animatesDrop = true
    //                // 大頭針的顏色
    //                pinView?.pinTintColor =
    //                    UIColor.green
    //                // 這邊將額外視圖的右邊視圖設為一個按鈕
    //                pinView?.rightCalloutAccessoryView =
    //                    UIButton(type: .detailDisclosure)
    //
    //                pinView?.pinTintColor = color[Int(arc4random_uniform(UInt32(color.count)))]
    //
    //            } else {
    //                pinView?.annotation = annotation
    //            }
    //
    //            return pinView
    //        }
    //
    //
    ////        var pointView : MKPinAnnotationView? = map.dequeueReusableAnnotationView(withIdentifier: "CustomerPoint") as? MKPinAnnotationView;
    ////
    ////        print("\(pointView)")
    ////
    ////        if(pointView == nil){
    ////
    ////            pointView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "CustomerPoint");
    ////
    ////            pointView?.canShowCallout = true;
    ////        }
    ////        //pointView!.pinTintColor = UIColor.green
    ////
    ////
    ////        return pointView;
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
