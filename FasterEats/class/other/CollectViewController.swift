//
//  CollectViewController.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/30.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class CollectViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarItem = UITabBarItem(title: "收藏", image: UIImage(named: "mainCollect"), tag: 0)
        if let items = tabBarController?.tabBar.items {
            let tabBarImages:[String] = ["mainMenu", "mainOrder", "mainCollect", "mainUser"]
            let tabBarTitle:[String] = ["精選推薦", "我的預訂", "收藏", "會員中心"]
            let tabBarSelectImage:[String] = ["selectMenu", "selectOrder", "selectCollect", "selectUser"]
            for i in 0..<items.count {
                let tabBarItem = items[i]
                let tabBarImage = tabBarImages[i]
                tabBarItem.image = UIImage(named: tabBarImage)//.withRenderingMode(.alwaysOriginal)
                tabBarItem.title = tabBarTitle[i]
                tabBarItem.selectedImage = UIImage(named: tabBarSelectImage[i])
            }
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
