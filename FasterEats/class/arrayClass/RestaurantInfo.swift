//
//  RestaurantInfo.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/25.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import Foundation

class RestaurantInfo {
    var image:String = ""
    var name:String = ""
    var star:String = ""
    var price:String = ""
    var type:String = ""
    var far:String = ""
    var seats:String = ""
    var phone:String = ""
    var time:String = ""
    var address:String = ""
    var additional:String = ""
    var waitTime:String = ""
    var isOpen:Int = 0
    var seatState:Int = 0
    
    init(image: String, name: String, star: String, price: String, type:String, far: String, seats: String, phone: String, time: String, address: String, additional: String, waitTime: String, isOpen: Int, seatState: Int) {
        self.image = image
        self.name = name
        self.star = star
        self.price = price
        self.type = type
        self.far = far
        self.seats = seats
        self.phone = phone
        self.time = time
        self.address = address
        self.additional = additional
        self.waitTime = waitTime
        self.isOpen = isOpen
        self.seatState = seatState
    }
    
}
