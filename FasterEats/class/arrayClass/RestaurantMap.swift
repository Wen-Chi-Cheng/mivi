//
//  RestaurantMap.swift
//  FasterEats
//
//  Created by TingKuan Lin on 2018/8/27.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import Foundation
import UIKit

class RestaurantMap{
    
    var id:Int = -1
    var name:String = ""
    var address:String = ""
    var lat:Double = 0.0
    var lon:Double = 0.0
    var color:UIColor
    var type:String = ""
    
    var price:String = ""
    var dist:String = ""
    var seat:String = ""
    
    
    init(id:Int, name:String, address:String, lat:Double, lon:Double, color:UIColor, type:String, price:String, dist:String, seat:String) {
        self.id = id
        self.name = name
        self.address = address
        self.lat = lat
        self.lon = lon
        self.color = color
        self.type = type
        
        self.price = price
        self.dist = dist
        self.seat = seat
    }
    
}
