//
//  Restaurant.swift
//  FasterEats
//
//  Created by TingKuan Lin on 2018/8/23.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import Foundation

class Restaurant {
    
    var image = ""
    var name = ""
    var star = ""
    var price = ""
    var type = ""
    var far = ""
    var seat = ""
    var wait = ""
    
    var isOpen:Int
    var seatState:Int
    
//    var location = ""
//    var phone = ""
//    var isVisited = false
    
    init(image:String, name: String, star:String, price:String, type: String, far: String, seat: String, isOpen:Int, seatState:Int, wait:String) {
        self.image = image
        self.name = name
        self.star = star
        self.price = price
        self.type = type
        self.far = far
        self.seat = seat
        self.wait = wait
        self.isOpen = isOpen
        self.seatState = seatState
        
    }
    
}
