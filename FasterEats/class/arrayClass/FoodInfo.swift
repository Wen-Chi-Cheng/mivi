//
//  FoodInfo.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/25.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import Foundation

class FoodInfo {
    //var id:Int = 0
    var image:String = ""
    var name:String = ""
    var description:String = ""
    var price = ""
    
    init(image: String, name: String, description: String, price: String) {
        //self.id = id
        self.image = image
        self.name = name
        self.description = description
        self.price = price
    }
    
}
