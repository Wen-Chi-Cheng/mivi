//
//  RestaurantTableViewCell.swift
//  FasterEats
//
//  Created by TingKuan Lin on 2018/8/24.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {

    
    @IBOutlet weak var myView: RestaurantInfoTableViewCell!
    
    @IBOutlet weak var image_imgV: UIImageView!

    @IBOutlet weak var name_lbl: UILabel!
    
    @IBOutlet weak var star_imgV: UIImageView!
    
    @IBOutlet weak var star_price_lbl: UILabel!
    
    @IBOutlet weak var seat_imgV: UIImageView!
    
    @IBOutlet weak var seat_lbl: UILabel!
    
    @IBOutlet weak var waiting_board: UIImageView!
    
    @IBOutlet weak var wait_lbl: UILabel!
    
    @IBOutlet weak var wait_min_lbl: UILabel!
    
    @IBOutlet weak var wait_time_lbl: UILabel!
    
    @IBOutlet weak var type_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
