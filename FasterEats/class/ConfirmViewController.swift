//
//  ConfirmViewController.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/30.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class ConfirmViewController: UIViewController {

    var resName:String = ""
    var star:String = ""
    var date:String = ""
    var time:String = ""
    var peopleNum:String = ""
    var meal:String = ""
    var price:Int = 0
    
    @IBOutlet weak var conResName: UILabel!
    @IBOutlet weak var conStar: UILabel!
    @IBOutlet weak var conDate: UILabel!
    @IBOutlet weak var conTime: UILabel!
    @IBOutlet weak var conPeopleNumber: UILabel!
    @IBOutlet weak var conMeal: UILabel!
    @IBOutlet weak var conPrice: UILabel!
    
    @IBAction func goToOrderVC(_ sender: Any) {
        //let tabBar = UITabBarController()
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "mainVC")
        //let myOrderVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderViewController
        
        //tabBar.present(myOrderVC, animated: true, completion: nil)
        //nav.pushViewController(bookingAndOrderingVC, animated: false)
        //self.present(nav, animated: true, completion: nil)
        
        present(controller!, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //let bookingAndOrderingVC = self.storyboard?.instantiateViewController(withIdentifier: "BookingAndOrderingVC") as! BookingAndOrderingViewController
        
        conMeal.lineBreakMode = NSLineBreakMode.byWordWrapping
        conMeal.numberOfLines = 0
        
        conResName.text = resName
        conStar.text = star
        conDate.text = date
        conTime.text = time
        conPeopleNumber.text = peopleNum
        conMeal.text = meal
        conPrice.text = "\(price)"
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
