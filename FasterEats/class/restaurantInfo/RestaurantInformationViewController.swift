//
//  RestaurantInformationViewController.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/25.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit
import Firebase

class RestaurantInformationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var resInfoTableView: UITableView!
    
    lazy var ref = Database.database().reference()
    var orderResImageView:String = ""
    
    var restaurantIndex:Int = 0
    var count:Int = 0
    var foodIndex:Int = 0
    //var foodCount:Int = 0
    
    var resImageView:String = ""
    var restaurantName:String = ""
    var star:String = ""
    var date:String = "今天的日期"
    var time:String = "訂位的時間"
    var peopleNumber:String = ""
    var meal:[String] = [String](repeating: "", count: 50)
    var mealPrice:[String] = [String](repeating: "", count: 50)
    var mealCount:[String] = [String](repeating: "", count: 50)
//    var restaurantsInfo:[RestaurantInfo] = [RestaurantInfo](repeating: RestaurantInfo(image: "", name: "", star: "", price: "", type: "", far: "", seats: "", phone: "", time: "", address: "", additional: ""), count: 50)
//    var foodsInfo:[FoodInfo] = [FoodInfo](repeating: FoodInfo(image: "", name: "", description: "", price: ""), count: 50)
    
    var restaurantsInfo:[RestaurantInfo] = [
        RestaurantInfo(image: "image1", name: "name1", star: "star1", price: "price1", type: "type1", far: "far1", seats: "seat1", phone: "phone1", time: "time1", address: "address1", additional: "additional1", waitTime: "wait1", isOpen: 0, seatState: 0),
        RestaurantInfo(image: "image2", name: "name2", star: "star2", price: "price2", type: "type2", far: "far2", seats: "seat2", phone: "phone2", time: "time2", address: "address2", additional: "additional2", waitTime: "wait2", isOpen: 0, seatState: 0),
        RestaurantInfo(image: "image3", name: "name3", star: "star3", price: "price3", type: "type3", far: "far3", seats: "seat3", phone: "phone3", time: "time3", address: "address3", additional: "additional3", waitTime: "wait3", isOpen: 0, seatState: 0),
        RestaurantInfo(image: "image4", name: "name4", star: "star4", price: "price4", type: "type4", far: "far4", seats: "seat4", phone: "phone4", time: "time4", address: "address4", additional: "additional4", waitTime: "wait4", isOpen: 0, seatState: 0)
    ]
    
    var foodsInfo:[FoodInfo] = [
        FoodInfo(image: "image1", name: "name1", description: "description1", price: "price1"),
        FoodInfo(image: "image2", name: "name2", description: "description2", price: "price2"),
        FoodInfo(image: "image3", name: "name3", description: "description3", price: "price3"),
        FoodInfo(image: "image4", name: "name4", description: "description4", price: "price4")
    ]
  /*
    var restaurantsInfo:[RestaurantInfo] = [
        RestaurantInfo(image: "food.jpeg", name: "義大皇家酒店星亞自助餐", star: "4.7", price: "$~$$", type: "餐廳", far: "350m", seats: "有空位", phone: "07-6568166#21205", time: "10:00am ~ 11:30pm", address: "840高雄市大樹區學城路1段153號義大皇家酒店LB大廳樓層", additional: "冷氣、插頭、WiFi"),
        RestaurantInfo(image: "food.jpeg", name: "義大皇家酒店皇樓", star: "3.8", price: "$~$$", type: "港式飲茶餐廳", far: "780m", seats: "等待15分鐘", phone: "07-6568166#21402", time: "10:30am ~ 9:30pm", address: "840高雄市大樹區學城路1段153號義大皇家酒店LB大廳樓層", additional: "冷氣、插頭、WiFi"),
        RestaurantInfo(image: "food.jpeg", name: "皇港茶餐廳", star: "4.2", price: "$~$$", type: "餐廳", far: "300m", seats: "等待10分鐘", phone: "07-6150011", time: "11:00am ~ 11:30pm", address: "827高雄市燕巢區義大路1號", additional: "冷氣、插頭、WiFi、車位、寵物友善"),
        RestaurantInfo(image: "food.jpeg", name: "叁合院", star: "4.8", price: "$~$$", type: "臺菜餐廳", far: "1200m", seats: "等待20分鐘", phone: "07-6568222", time: "11:00am ~ 11:30pm", address: "840高雄市大樹區學城路1段12號A區5樓", additional: "冷氣、插頭、WiFi、車位、親子餐廳"),
        RestaurantInfo(image: "food.jpeg", name: "1010湘料理", star: "3.8", price: "$~$$", type: "湘菜餐廳", far: "800m", seats: "等待17分鐘", phone: "07-6569009", time: "10:30am ~ 10:30pm", address: "840高雄市大樹區學城路1段12號A區5樓", additional: "冷氣、插頭、WiFi、車位")
        ]
    
    var foodsInfo:[FoodInfo] = [
        FoodInfo(image: "food.jpeg", name: "食物1", description: "食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1食物1", price: "120"),
        FoodInfo(image: "food.jpeg", name: "食物2", description: "食物2食物2食物2食物2食物2食物2食物2食物2食物2食物2食物2食物2食物2食物2食物2", price: "80"),
        FoodInfo(image: "food.jpeg", name: "食物3", description: "食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3食物3", price: "360"),
        FoodInfo(image: "food.jpeg", name: "食物4", description: "食物4食物4食物4食物4食物4食物4食物4食物4食物4食物4食物4食物4", price: "320"),
        FoodInfo(image: "food.jpeg", name: "食物5", description: "食物5食物5食物5食物5食物5食物5食物5食物5", price: "120"),
        FoodInfo(image: "food.jpeg", name: "食物6", description: "食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6食物6", price: "220"),
        FoodInfo(image: "food.jpeg", name: "食物7", description: "食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7食物7", price: "480"),
    ]
 */
    //var resFoodInfo:[]
    
//    @IBOutlet weak var booking: UIButton!
//    @IBOutlet weak var bookingAndOrdering: UIButton!
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func collect(_ sender: Any) {
    }
    
    
    @IBAction func bookingAndOrdering(_ sender: Any) {
        var nav = UINavigationController()
        
        let bookingAndOrderingVC = self.storyboard?.instantiateViewController(withIdentifier: "BookingAndOrderingVC") as! BookingAndOrderingViewController
        
        
        bookingAndOrderingVC.resImageView = self.orderResImageView
        bookingAndOrderingVC.restaurantName = self.restaurantName
        bookingAndOrderingVC.star = self.star
        bookingAndOrderingVC.date = self.date
        bookingAndOrderingVC.time = self.time
        bookingAndOrderingVC.peopleNumber = self.peopleNumber
        bookingAndOrderingVC.meal = self.meal
        bookingAndOrderingVC.mealPrice = self.mealPrice
        bookingAndOrderingVC.mealCount = self.mealCount
        
//        print("people number: \(peopleNumber)")
//        for index in 0...meal.count {
//            if meal[index] == ""{
//                break;
//            }
//            else{
//                print("meal: \(meal[index])")
//                print("mealPrice: \(mealPrice[index])")
//                print("mealCount: \(mealCount[index])")
//            }
//        }
        
            
        nav.pushViewController(bookingAndOrderingVC, animated: false)
        self.present(nav, animated: true, completion: nil)
    }
    
    
//    @objc func pushBookingAndOrderingVC(){
//        let bookingAndOrderingVC = self.storyboard?.instantiateViewController(withIdentifier: "BookingAndOrderingVC") as! BookingAndOrderingViewController
//
//        self.navigationController?.pushViewController(bookingAndOrderingVC, animated: true)
//    }
    
//    func application(_ application: UIApplication,
//                     didFinishLaunchingWithOptions launchOptions:
//        [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        var navi = UINavigationController()
//        var firstVC = self.window?.rootViewController
//        navi.pushViewController(firstVC!, animated: false)
//        self.window?.rootViewController = navi
//        return true
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("res restaurantIndex : \(restaurantIndex)")
        
        var resCount:String = "res\(restaurantIndex)"
        print("in database: \(resCount)")
        ref.child("restaurantInfo").child(resCount).observeSingleEvent(of: .value, with: { (snapshot) in
            //print("get database value\(snapshot.value)")
            let value:NSDictionary = (snapshot.value as? NSDictionary)!
            self.orderResImageView = value["image"] as! String
            self.restaurantsInfo[self.restaurantIndex].name = value["name"] as! String
            self.restaurantsInfo[self.restaurantIndex].star = value["star"] as! String
            self.restaurantsInfo[self.restaurantIndex].price = value["price"] as! String
            self.restaurantsInfo[self.restaurantIndex].type = value["type"] as! String
            self.restaurantsInfo[self.restaurantIndex].far = value["far"] as! String
            self.restaurantsInfo[self.restaurantIndex].seats = value["seat"] as! String
            self.restaurantsInfo[self.restaurantIndex].phone = value["phone"] as! String
            self.restaurantsInfo[self.restaurantIndex].time = value["time"] as! String
            self.restaurantsInfo[self.restaurantIndex].address = value["address"] as! String
            self.restaurantsInfo[self.restaurantIndex].additional = value["additional"] as! String
            self.restaurantsInfo[self.restaurantIndex].waitTime = value["waitTime"] as! String
            self.restaurantsInfo[self.restaurantIndex].isOpen = value["isOpen"] as! Int
            self.restaurantsInfo[self.restaurantIndex].seatState = value["seatState"] as! Int
 
            print("database res image: \(self.orderResImageView)\n database res name: \(self.restaurantsInfo[self.restaurantIndex].name)\n database res star: \(self.restaurantsInfo[self.restaurantIndex].star)\n database res price: \(self.restaurantsInfo[self.restaurantIndex].price)\n database res type: \(self.restaurantsInfo[self.restaurantIndex].type)\n database res far: \(self.restaurantsInfo[self.restaurantIndex].far)\n database res seat: \(self.restaurantsInfo[self.restaurantIndex].seats)\n database res phone: \(self.restaurantsInfo[self.restaurantIndex].phone)\n database res time: \(self.restaurantsInfo[self.restaurantIndex].time)\n database res address: \(self.restaurantsInfo[self.restaurantIndex].address)\n database res additional: \(self.restaurantsInfo[self.restaurantIndex].additional)\n database res waitTime:  \(self.restaurantsInfo[self.restaurantIndex].waitTime)\n database res isOpen:  \(self.restaurantsInfo[self.restaurantIndex].isOpen)\n database res seatState:  \(self.restaurantsInfo[self.restaurantIndex].seatState)\n")
            
            //self.resInfoTableView.reloadData()
        })
        ref.child("foodInfo").child(resCount).observeSingleEvent(of: .value, with: { (snapshot) in
            //print("get database value\(snapshot.value)")
            let value:NSDictionary = (snapshot.value as? NSDictionary)!
            self.restaurantsInfo[self.restaurantIndex].image = value["resImage"] as! String
            
            print("database res image: \(self.restaurantsInfo[self.restaurantIndex].image)\n")
            if self.resInfoTableView != nil{
                self.resInfoTableView.reloadData()
            }
        })
        
        for index in 0...3{
            var foodCount:String = "food\(index)"
            print("in database: \(foodCount)")
            ref.child("foodInfo").child(resCount).child(foodCount).observeSingleEvent(of: .value, with: { (snapshot) in
                print("food index:\(index)")
                let value:NSDictionary = (snapshot.value as? NSDictionary)!
                self.foodsInfo[index].image = value["image"] as! String
                self.foodsInfo[index].name = value["name"] as! String
                self.foodsInfo[index].description = value["description"] as! String
                self.foodsInfo[index].price = value["price"] as! String
                
                print("database res image: \(self.foodsInfo[index].image)\n database res name: \(self.foodsInfo[index].name)\n database res description: \(self.foodsInfo[index].description)\n database res price: \(self.foodsInfo[index].price)\n ")
                
                self.meal[index] = self.foodsInfo[index].name
                self.mealPrice[index] = self.foodsInfo[index].price
                
                if self.resInfoTableView != nil{
                    self.resInfoTableView.reloadData()
                }
            })
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        count = 0
//        for index in 0..<foodsInfo.count {
//            if foodsInfo[index].id == restaurantIndex {
//                count += 1
//                foodIndex = index
//            }
//        }
        //print("food count : \(foodsInfo.count)")
        //return foodsInfo.count + 2
        //return restaurantsInfo.count + 2
        return 6
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        foodIndex = indexPath.row
        print("in select foodIndex: \(foodIndex)")
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 900
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            var cellResInfo:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "restaurantInfoCell")
            tableView.rowHeight = 844
            //var cellResInfo:UITableViewCell? = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "restaurantInfoCell")
            let myCellResInfo:RestaurantInfoTableViewCell = cellResInfo as! RestaurantInfoTableViewCell
            
            //var myCellResInfo:RestaurantInfoTableViewCell = RestaurantInfoTableViewCell()
            print("\(restaurantsInfo[restaurantIndex].name)")
            myCellResInfo.restaurantImage.image = UIImage(named: restaurantsInfo[restaurantIndex].image)
            myCellResInfo.restaurantNameLabel.text = restaurantsInfo[restaurantIndex].name
            myCellResInfo.starLabel.text = restaurantsInfo[restaurantIndex].star
            myCellResInfo.priceLabel.text = restaurantsInfo[restaurantIndex].price
            myCellResInfo.typeLabel.text = restaurantsInfo[restaurantIndex].type
            myCellResInfo.farLabel.text = restaurantsInfo[restaurantIndex].far
            myCellResInfo.seatLabel.text = restaurantsInfo[restaurantIndex].seats
            myCellResInfo.phoneLabel.text = restaurantsInfo[restaurantIndex].phone
            myCellResInfo.timeLabel.text = restaurantsInfo[restaurantIndex].time
            myCellResInfo.addressLabel.text = restaurantsInfo[restaurantIndex].address
            myCellResInfo.additionalLabel.text = restaurantsInfo[restaurantIndex].additional
            
            if restaurantsInfo[restaurantIndex].waitTime == "" {
                myCellResInfo.waitTimeImageView.isHidden = true
                myCellResInfo.inResLabel.isHidden = true
                myCellResInfo.waitTimeLabel.isHidden = true
            }
            else {
                myCellResInfo.waitTimeImageView.isHidden = false
                myCellResInfo.inResLabel.isHidden = false
                myCellResInfo.waitTimeLabel.isHidden = false
            }
            

            if restaurantsInfo[restaurantIndex].isOpen == 0{
                myCellResInfo.starImageView.image = UIImage(named: "shape_gray")
                myCellResInfo.seatPointLabel.image = UIImage(named: "oval_grey")
            }
            else{
                myCellResInfo.starImageView.image = UIImage(named: "shape")
                if restaurantsInfo[restaurantIndex].seatState == 1 {
                    myCellResInfo.seatPointLabel.image = UIImage(named: "oval_green")
                }else if restaurantsInfo[restaurantIndex].seatState == 0 {
                    myCellResInfo.seatPointLabel.image = UIImage(named: "oval_orange")
                }else {
                    myCellResInfo.seatPointLabel.image = UIImage(named: "oval_red")
                }
            }
            
            
            myCellResInfo.rVC = self
            
            let now:Date = Date()
            let dateFormat:DateFormatter = DateFormatter()
            dateFormat.dateFormat = "MM月dd日"
            let dateString:String = dateFormat.string(from: now)
            print("現在時間：\(dateString)")
            
            resImageView = restaurantsInfo[restaurantIndex].image
            restaurantName = restaurantsInfo[restaurantIndex].name
            star = restaurantsInfo[restaurantIndex].star
            date = dateString
            time = "前面所選的時間區塊"
            //peopleNumber = "\(myCellResInfo.peopleNumberCount)"
            //meal = ["有點的餐：", "meal 1", "meal 2", "meal 3", "meal 4", "meal 5"]
            //mealPrice = ["餐點的錢：", "80", "150", "100", "120", "90"]
            
            
            return cellResInfo!
        }
        else if indexPath.row >= 1 && indexPath.row <= 4 {
            var cellFoodInfo:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "foodInfoCell")
            tableView.rowHeight = 255
            
            //var cellFoodInfo:FoodInfoTableViewCell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "foodInfoCell") as! FoodInfoTableViewCell
            var myCellFoodInfo:FoodInfoTableViewCell = cellFoodInfo as! FoodInfoTableViewCell
            
            myCellFoodInfo.rVC = self
            
            myCellFoodInfo.foodImageView.image = UIImage(named: foodsInfo[indexPath.row - 1].image)
            myCellFoodInfo.foodNameLabel.text = foodsInfo[indexPath.row - 1].name
            myCellFoodInfo.foodDescriptionLabel.text = foodsInfo[indexPath.row - 1].description
            myCellFoodInfo.foodPriceLabel.text = "$\(foodsInfo[indexPath.row - 1].price)"
            myCellFoodInfo.index = indexPath.row
            
//            meal[indexPath.row - 1] = myCellFoodInfo.foodNameLabel.text!
//            mealPrice[indexPath.row - 1] = myCellFoodInfo.foodPriceLabel.text!
            //mealCount[indexPath.row - 1] = "\(myCellFoodInfo.foodCount)"
            
            
            //print("food image: \(self.foodsInfo[indexPath.row - 1].image)\n food name: \(self.foodsInfo[indexPath.row - 1].name)\n food description: \(self.foodsInfo[indexPath.row - 1].description)\n food price: \(self.foodsInfo[indexPath.row - 1].price)\n ")
            
            
            return cellFoodInfo!
        }
        else{
            var cellBooking:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "bookingCell")
            tableView.rowHeight = 50
            //var cellBooking:BookingTableViewCell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "bookingCell") as! BookingTableViewCell
            var myCellBooking:BookingTableViewCell = cellBooking as! BookingTableViewCell
            //myCellBooking.foodNameLabel.text = "foodNameLabel"
            
            return cellBooking!
        }
        
    }
    
    func updateItem(item:Int, cellID:Int){
        mealCount[cellID - 1] = "\(item)"
        print("meal id: \(cellID)  meal count: \(mealCount[cellID - 1])")
    }
    
    func updatePeopleNumber(number:Int){
        peopleNumber = "\(number)"
        print("people number: \(peopleNumber)")
    }
    
    func timeSelect(timeSelect: String){
        time = timeSelect
        print("select time: \(timeSelect)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
