//
//  FoodInfoTableViewCell.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/25.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class FoodInfoTableViewCell: UITableViewCell {

    var foodCount:Int = 0
    var rVC:RestaurantInformationViewController?
    var index:Int = 0
    
    @IBOutlet weak var foodNameLabel: UILabel!
    @IBOutlet weak var foodDescriptionLabel: UILabel!
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var foodPriceLabel: UILabel!
    @IBOutlet weak var foodCountLabel: UILabel!
    
    
    @IBAction func minusFoodButton(_ sender: Any) {
        if foodCount != 0 {
            foodCount -= 1
            foodCountLabel.text = "\(foodCount)"
            print("in cell ==>cellID: \(index), food count: \(foodCount)")
            rVC?.updateItem(item: foodCount, cellID: index)
        }
    }
    
    @IBAction func addFoodButton(_ sender: Any) {
        foodCount += 1
        foodCountLabel.text = "\(foodCount)"
        print("in cell ==>cellID: \(index), food count: \(foodCount)")
        rVC?.updateItem(item: foodCount, cellID: index)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //foodDescriptionLabel
        foodDescriptionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        foodDescriptionLabel.numberOfLines = 0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
