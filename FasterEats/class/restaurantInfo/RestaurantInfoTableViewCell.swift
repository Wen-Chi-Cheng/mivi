//
//  RestaurantInfoTableViewCell.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/25.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class RestaurantInfoTableViewCell: UITableViewCell {
    
    var peopleNumberCount:Int = 0
    var rVC:RestaurantInformationViewController?
    
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var farLabel: UILabel!
    @IBOutlet weak var seatLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var additionalLabel: UILabel!
    @IBOutlet weak var restaurantImage: UIImageView!
    @IBOutlet weak var peopleNumberLabel: UILabel!
    @IBOutlet weak var waitTimeImageView: UIImageView!
    @IBOutlet weak var inResLabel: UILabel!
    @IBOutlet weak var waitTimeLabel: UILabel!
    @IBOutlet weak var seatPointLabel: UIImageView!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var timeButton1: UIButton!
    @IBOutlet weak var timeButton2: UIButton!
    @IBOutlet weak var timeButton3: UIButton!
    @IBOutlet weak var timeButton4: UIButton!
    @IBOutlet weak var timeButton5: UIButton!
    @IBOutlet weak var timeButton6: UIButton!
    
    
    @IBAction func peopleNumberMiuns(_ sender: Any) {
        if peopleNumberCount != 0 {
            peopleNumberCount -= 1
            peopleNumberLabel.text = "\(peopleNumberCount)"
            print("in cell ==> people number: \(peopleNumberCount)")
            rVC?.updatePeopleNumber(number: peopleNumberCount)
        }
    }
    
    @IBAction func peopleNumberPlus(_ sender: Any) {
        peopleNumberCount += 1
        peopleNumberLabel.text = "\(peopleNumberCount)"
        print("in cell ==> people number: \(peopleNumberCount)")
        rVC?.updatePeopleNumber(number: peopleNumberCount)
    }
    
    @IBAction func timeButtonAction1(_ sender: Any) {
        timeButton1.setBackgroundImage(UIImage(named: "rectangleRed"), for: .normal)
        timeButton2.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton3.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton4.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton5.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton6.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        
        timeButton1.setTitleColor(UIColor.white, for: .normal)
        timeButton2.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton3.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton4.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton5.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton6.setTitleColor(UIColor.greyishBrown, for: .normal)
        
        rVC?.timeSelect(timeSelect: "12:00~12:30")
    }
    @IBAction func timeButtonAction2(_ sender: Any) {
        timeButton1.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton2.setBackgroundImage(UIImage(named: "rectangleRed"), for: .normal)
        timeButton3.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton4.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton5.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton6.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        
        timeButton1.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton2.setTitleColor(UIColor.white, for: .normal)
        timeButton3.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton4.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton5.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton6.setTitleColor(UIColor.greyishBrown, for: .normal)
        
        rVC?.timeSelect(timeSelect: "12:30~13:00")
    }
    @IBAction func timeButtonAction3(_ sender: Any) {
        timeButton1.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton2.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton3.setBackgroundImage(UIImage(named: "rectangleRed"), for: .normal)
        timeButton4.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton5.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton6.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        
        timeButton1.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton2.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton3.setTitleColor(UIColor.white, for: .normal)
        timeButton4.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton5.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton6.setTitleColor(UIColor.greyishBrown, for: .normal)
        
        rVC?.timeSelect(timeSelect: "13:00~13:30")
    }
    @IBAction func timeButtonAction4(_ sender: Any) {
        timeButton1.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton2.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton3.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton4.setBackgroundImage(UIImage(named: "rectangleRed"), for: .normal)
        timeButton5.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton6.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        
        timeButton1.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton2.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton3.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton4.setTitleColor(UIColor.white, for: .normal)
        timeButton5.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton6.setTitleColor(UIColor.greyishBrown, for: .normal)
        
        rVC?.timeSelect(timeSelect: "13:30~14:00")
    }
    @IBAction func timeButtonAction5(_ sender: Any) {
        timeButton1.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton2.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton3.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton4.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton5.setBackgroundImage(UIImage(named: "rectangleRed"), for: .normal)
        timeButton6.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        
        timeButton1.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton2.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton3.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton4.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton5.setTitleColor(UIColor.white, for: .normal)
        timeButton6.setTitleColor(UIColor.greyishBrown, for: .normal)
        
        rVC?.timeSelect(timeSelect: "14:00~14:30")
    }
    @IBAction func timeButtonAction6(_ sender: Any) {
        timeButton1.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton2.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton3.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton4.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton5.setBackgroundImage(UIImage(named: "rectangleWhite"), for: .normal)
        timeButton6.setBackgroundImage(UIImage(named: "rectangleRed"), for: .normal)
        
        timeButton1.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton2.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton3.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton4.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton5.setTitleColor(UIColor.greyishBrown, for: .normal)
        timeButton6.setTitleColor(UIColor.white, for: .normal)
        
        rVC?.timeSelect(timeSelect: "14:30~15:00")
    }
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
