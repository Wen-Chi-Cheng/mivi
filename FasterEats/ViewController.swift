//
//  ViewController.swift
//  FasterEats
//
//  Created by 文琪 on 2018/8/22.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import ZKProgressHUD

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate {

    lazy var ref = Database.database().reference()
    
    //var restaurants:[Restaurant] = [Restaurant](repeating: Restaurant(image: "", name: "", star: "", price: "", type: "", far: "", seat: ""), count: 50)
    /*
    var restaurants:[Restaurant] = [
        Restaurant(image: "image1", name: "name1", star: "s1", price: "p1", type: "t1", far: "far1", seat: "seat1"),
        Restaurant(name: "義大皇家酒店星亞自助餐", type: "餐廳", location: "840高雄市大樹區學城路1段153號義大皇家酒店LB大廳樓層", phone: "07-6568166#21205", image: "food.jpeg", isVisited: false),
        Restaurant(name: "義大皇家酒店皇樓", type: "港式飲茶餐廳", location: "840高雄市大樹區學城路1段153號義大皇家酒店LB大廳樓層", phone: "07-6568166#21402", image: "food.jpeg", isVisited: false),
        Restaurant(name: "皇港茶餐廳", type: "餐廳", location: "827高雄市燕巢區義大路1號", phone: "07-6150011", image: "food.jpeg", isVisited: false),
        Restaurant(name: "叁合院", type: "臺菜餐廳", location: "840高雄市大樹區學城路1段12號A區5樓", phone: "07-6568222", image: "food.jpeg", isVisited: false),
        Restaurant(name: "1010湘料理", type: "湘菜餐廳", location: "840高雄市大樹區學城路1段12號A區5樓", phone: "07-6569009", image: "food.jpeg", isVisited: false),
        ]
    */
    var restaurants:[Restaurant] = [
        Restaurant(image: "res_img_0", name: "name1", star: "s1", price: "p1", type: "t1", far: "far1", seat: "seat1", isOpen:0, seatState:1, wait:"-1"),
        Restaurant(image: "res_img_1", name: "name2", star: "s2", price: "p2", type: "t2", far: "far2", seat: "seat2", isOpen:0, seatState:1, wait:"-1"),
        Restaurant(image: "res_img_2", name: "name3", star: "s3", price: "p3", type: "t3", far: "far3", seat: "seat3", isOpen:0, seatState:1, wait:"-1"),
        Restaurant(image: "res_img_3", name: "name4", star: "s4", price: "p4", type: "t4", far: "far4", seat: "seat4", isOpen:0, seatState:1, wait:"-1")
        ]
    
    
    
    let fullScreenSize = UIScreen.main.bounds.size
    
    var sceneArray = ["icon.png","scene2","scene3","scene4","scene5","scene6"]
    
    
    @IBAction func GotoMapView(_ sender: Any) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MapVC")
        //let controller = self.storyboard?.instantiateViewController(withIdentifier: "map_test")
//        if controller != nil {
//            present(controller!, animated: true, completion: nil)
//        }
        
        present(controller!, animated: true, completion: nil)
        
    }
    
  
    var myScrollImageview: UIImageView!
    
    var needDatabase:Bool = true
    
    
    @IBOutlet weak var myView: UIView!
    
    @IBOutlet weak var RestaurantTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarItem = UITabBarItem(title: "精選推薦", image: UIImage(named: "mainMenu"), tag: 0)
        
        if let items = tabBarController?.tabBar.items {
            let tabBarImages:[String] = ["mainMenu", "mainOrder", "mainCollect", "mainUser"]
            let tabBarTitle:[String] = ["精選推薦", "我的預訂", "收藏", "會員中心"]
            let tabBarSelectImage:[String] = ["selectMenu", "selectOrder", "selectCollect", "selectUser"]
            for i in 0..<items.count {
                let tabBarItem = items[i]
                let tabBarImage = tabBarImages[i]
                tabBarItem.image = UIImage(named: tabBarImage)//.withRenderingMode(.alwaysOriginal)
                tabBarItem.title = tabBarTitle[i]
                tabBarItem.selectedImage = UIImage(named: tabBarSelectImage[i])
            }
        }
        
        ZKProgressHUD.setMaskStyle(ZKProgressHUDMaskStyle.visible)
        ZKProgressHUD.setMaskBackgroundColor(UIColor.white)
        ZKProgressHUD.setEffectAlpha(0)
        ZKProgressHUD.setAnimationShowStyle (ZKProgressHUDAnimationShowStyle.fade)
        ZKProgressHUD.setAnimationStyle(ZKProgressHUDAnimationStyle.circle)
        ZKProgressHUD.show()
        
//        var myOrderVC:MyOrderViewController?
//        myOrderVC!.tabBarItem = UITabBarItem(title: "我的預訂", image: UIImage(named: "mainOrder"), tag: 0)
        
        for index in 0...3{
            var resCount:String = "res\(index)"
            //print("in database: \(resCount)")
            ref.child("restaurantInfo").child(resCount).observeSingleEvent(of: .value, with: { (snapshot) in
                //print("get database value\(snapshot.value)")
                print("in database: \(resCount)")
                
                let value:NSDictionary = (snapshot.value as? NSDictionary)!
                self.restaurants[index].image = value["image"] as! String
                self.restaurants[index].name = value["name"] as! String
                self.restaurants[index].star = value["star"] as! String
                self.restaurants[index].price = value["price"] as! String
                self.restaurants[index].type = value["type"] as! String
                self.restaurants[index].far = value["far"] as! String
                self.restaurants[index].seat = value["seat"] as! String
                self.restaurants[index].isOpen = value["isOpen"] as! Int
                self.restaurants[index].seatState = value["seatState"] as! Int
                self.restaurants[index].wait = value["wait"] as! String
                
                print("database res image: \(self.restaurants[index].image)\n database res name: \(self.restaurants[index].name)\n database res star: \(self.restaurants[index].star)\n database res price: \(self.restaurants[index].price)\n database res type: \(self.restaurants[index].type)\n database res far: \(self.restaurants[index].far)\n database res seat: \(self.restaurants[index].seat)\n ")
                
                if self.RestaurantTableView != nil{
                    self.RestaurantTableView.reloadData()
                }
            })
            //RestaurantTableView.reloadData()
            ZKProgressHUD.dismiss(3)
        }
        
        
        
        
        
        
        
        //ref.setValue("Test")

//
//        for index in 0...3{
//            var resCount:String = "res\(index)"
//        var resCount:String = "res\(0)"
//            print("in database: \(resCount)")
//
//        ref.observe(.value) { (snapshot) in
//            print("=====get value\(snapshot.value)")
//
//        }
        
//        ref.child("restaurantInfo").child(resCount).observe(.value) { (snapshot) in
//            print("=====get database value\(snapshot.value)")
//            let value:NSDictionary = (snapshot.value as? NSDictionary)!
//            self.restaurants[index].name = value["name"] as! String
//            print("database res name: \(self.restaurants[index].name)")
//        }
        
//            ref.child("restaurantInfo").child(resCount).observeSingleEvent(of: .value, with: { (snapshot) in
//                print("=====get database value\(snapshot.value)")
//                let value:NSDictionary = (snapshot.value as? NSDictionary)!
//                self.restaurants[0].name = value["name"] as! String
//                print("database res name: \(self.restaurants[0].name)")
//            })
        //}
        
//        ref.child("SpyGame").child("Group01").child("WhoseTurn").child("random").observeSingleEvent(of: .value, with: { (snapshot) in
//            print(snapshot.value)
//            let value:[Int] = (snapshot.value as? Array)!
//            print(value)
//            self.random = value
//            if self.random.contains(0){
//                var selectedIndex:Int = Int(arc4random_uniform(UInt32(self.random.count)))
//                print("SELECT \(selectedIndex)")
//                while(self.random[selectedIndex] != 0){
//                    selectedIndex = Int(arc4random_uniform(UInt32(self.random.count)))
//                }
//                for var index:Int in 0...self.whoseTurn.count-1{
//                    self.whoseTurn["player\(index)"] = false
//                }
//
//
//                self.random[selectedIndex] += 1
//                print("RANDOM \(selectedIndex)")
//                let indexStr:String = String(selectedIndex)
//                //let firstKey:String = Array(self.whoseTurn.keys)[selectedIndex]
//                self.whoseTurn["player\(indexStr)"] = true
//
//
//
//                print("Random \(self.random)")
//                print("Whose TURN \(self.whoseTurn)")
//
//            }
//            else{
//                self.changeVC()
//            }
        
        
        
        //RestaurantTableView.delegate = self
        //RestaurantTableView.dataSource = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let resInfoVC:RestaurantInformationViewController = segue.destination as! RestaurantInformationViewController
//        resInfoVC.restaurantIndex = resIndex
//        print("in prepare: resInfoVc = \(resInfoVC.restaurantIndex), resIndex = \(resIndex)")
//        //segueVC.segueLabel.text = inputField.text!
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count + 3
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("main restaurantIndex : \(indexPath.row)")
        //print(restaurants[indexPath.row - 3].name)
        
        if(indexPath.row > 2){
            let resInfo = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantInfoVC") as! RestaurantInformationViewController
        
            resInfo.restaurantIndex = indexPath.row - 3
            present(resInfo, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell? = nil
        
        if(indexPath.row == 0){
            tableView.rowHeight = 60
            cell = tableView.dequeueReusableCell(withIdentifier: "search")
            let searchCell:SearchTableViewCell = cell as! SearchTableViewCell
            return cell!
        }
        else if(indexPath.row == 1){
            tableView.rowHeight = 100
            cell = tableView.dequeueReusableCell(withIdentifier: "slider")
            let sliderCell:SliderTableViewCell = cell as! SliderTableViewCell
            
//            var scrollView:UIScrollView = UIScrollView(frame: tableView.bounds)
//            //scrollView.tintColor = UIColor.red
//            scrollView.backgroundColor = UIColor.gray
            
//            var collectionView:UICollectionView = UICollectionView(frame: tableView.bounds)
//            collectionView.backgroundColor = UIColor.gray
            
            //sliderCell.addSubview(collectionView)
            
            return cell!
        }
        else if(indexPath.row == 2){
            tableView.rowHeight = 70
            
            cell = tableView.dequeueReusableCell(withIdentifier: "labelCell")
            let labelCell:LabelTableViewCell = cell as! LabelTableViewCell
            labelCell.label.textColor = UIColor.fadedOrange
            
            return cell!
        }
        else{
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! RestaurantTableViewCell
            tableView.rowHeight = 220
            cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            var myCell:RestaurantTableViewCell = cell as! RestaurantTableViewCell
        
            myCell.myView.clipsToBounds = true
            myCell.myView.layer.cornerRadius = 15
            myCell.myView.layer.shadowOpacity = 0.9
            myCell.image_imgV.image = UIImage(named:restaurants[indexPath.row-3].image)
            myCell.image_imgV.clipsToBounds = true
            myCell.image_imgV.layer.cornerRadius = 18
            myCell.image_imgV.layer.shadowOpacity = 0.2
            myCell.name_lbl.text = restaurants[indexPath.row-3].name
            //myCell.address_lbl.text = restaurants[indexPath.row-3].location
            myCell.type_lbl.text = restaurants[indexPath.row-3].type + "\t |  " + restaurants[indexPath.row-3].far
            //myCell.type_lbl.text = test[indexPath.row]
            
           
            
            myCell.star_price_lbl.text = restaurants[indexPath.row-3].star + "\t NT" + restaurants[indexPath.row-3].price
            
            if restaurants[indexPath.row-3].isOpen == 0{
                myCell.star_imgV.image = UIImage(named: "shape_gray")
                myCell.seat_imgV.image = UIImage(named: "oval_grey")
                
                myCell.waiting_board.isHidden = true
                myCell.wait_lbl.isHidden = true
                myCell.wait_min_lbl.isHidden = true
                myCell.wait_time_lbl.isHidden = true
            }
            else{
                myCell.star_imgV.image = UIImage(named: "shape")
                if restaurants[indexPath.row-3].seatState == 1 {
                    myCell.seat_imgV.image = UIImage(named: "oval_green")
                }else if restaurants[indexPath.row-3].seatState == 0 {
                    myCell.seat_imgV.image = UIImage(named: "oval_orange")
                }else {
                    myCell.seat_imgV.image = UIImage(named: "oval_red")
                }
                if(restaurants[indexPath.row-3].wait != "-1"){
                    myCell.waiting_board.isHidden = false
                    myCell.wait_lbl.isHidden = false
                    myCell.wait_min_lbl.isHidden = false
                    myCell.wait_time_lbl.isHidden = false
                    myCell.wait_time_lbl.text = restaurants[indexPath.row-3].wait
                }
            }
            
            myCell.seat_lbl.text = restaurants[indexPath.row-3].seat
        
            return cell!
            
        }
       
    }
    
    


}

