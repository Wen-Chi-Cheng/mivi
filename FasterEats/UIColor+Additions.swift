//
//  UIColor+Additions.swift
//  吃飯大聲公
//
//  Generated on Zeplin. (2018/8/28).
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIColor {

  @nonobjc class var fadedOrange: UIColor {
    return UIColor(red: 243.0 / 255.0, green: 122.0 / 255.0, blue: 100.0 / 255.0, alpha: 1.0)
  }
    
    @nonobjc class var greyishBrown: UIColor {
        return UIColor(white: 70.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var warmGrey: UIColor {
        return UIColor(white: 158.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var warmGreyTwo: UIColor {
        return UIColor(white: 161.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var whiteGray: UIColor {
        return UIColor(white: 246.0 / 255.0, alpha: 1.0)
    }
}
