//
//  SliderTableViewCell.swift
//  FasterEats
//
//  Created by TingKuan Lin on 2018/8/27.
//  Copyright © 2018年 wenchi. All rights reserved.
//

import UIKit

class SliderTableViewCell: UITableViewCell, UIScrollViewDelegate{

    
    var collectCell:CategoryCollectionViewCell?
    
    var sliderState:[Int] = [Int](repeating: -1, count: 8)
    
    let fullScreenSize = UIScreen.main.bounds.size

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension SliderTableViewCell : UICollectionViewDataSource {

    
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderState.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath)

        collectCell = cell as! CategoryCollectionViewCell
        
        let index:Int = indexPath.row
        
        if sliderState[index] == 1 {
            let indexName = "category_" + String(index)
            print("\(indexName)")
            collectCell?.categoryImgV.image = UIImage(named: indexName)
        }else{
            let indexName = "category_src_" + String(index)
            print("\(indexName)")
            collectCell?.categoryImgV.image = UIImage(named: indexName)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let index:Int = indexPath.row
        
        print("======> select : \(index)")
        
        sliderState[index] = sliderState[index]*(-1)
        print("\(sliderState[index])")
        
        
//        if sliderState[index] == 1 {
//            let indexName = "category_" + String(index)
//            print("\(indexName)")
//            //collectCell?.categoryImgV.image = UIImage(named: indexName)
//        }else{
//            let indexName = "category_src_" + String(index)
//            print("\(indexName)")
//            //collectCell?.categoryImgV.image = UIImage(named: indexName)
//        }
        
        collectionView.reloadData()
        
    }


}
extension SliderTableViewCell : UICollectionViewDelegateFlowLayout {

     func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {

        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 6
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }




}


